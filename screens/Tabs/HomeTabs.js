import React, { Component } from 'react'
import { Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,FooterTab } from 'native-base';
import { TouchableOpacity } from 'react-native';
 import Home from"./HomeSwipers";
import Notifications from "./notifications";
import Messages from "./messages";
import Profile from "./profile"
import Add_ad from "./add_ad";

import { connect } from "react-redux";

   class HomeTabs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tab1: true,
            tab2: false,
            tab3: false,
            tab4: false,
            tab5: false

        };
    }
    toggleTab1() {
        this.setState({
            tab1: true,
            tab2: false,
            tab3: false,
            tab4: false,
            tab5: false

        });
    }
    toggleTab2() {
        this.setState({
            tab1: false,
            tab2: true,
            tab3: false,
            tab4: false,
            tab5: false

        });
    }
    toggleTab3() {
        this.setState({
            tab1: false,
            tab2: false,
            tab3: true,
            tab4: false,
            tab5: false

        });

    }
    toggleTab4() {
        this.setState({
            tab1: false,
            tab2: false,
            tab3: false,
            tab4: true,
            tab5: false
        });
    }
    toggleTab5() {
        this.setState({
            tab1: false,
            tab2: false,
            tab3: false,
            tab4: false,
            tab5: true


        });
    }

  render() {
    return (

        <Container>

              <Content >
                  {this.state.tab1 ? (
                      <Home  navigation={this.props.navigation} />
                      ) : null}
                  {this.state.tab2 ? (
                      <Messages navigation={this.props.navigation} />
                      ) : null}
                  {this.state.tab3 ? (
                      <Notifications navigation={this.props.navigation} />
                      ) : null}
                  {this.state.tab4 ? (
                      <Profile navigation={this.props.navigation} />
                      ) : null}
                  {this.state.tab5 ? (
                      <Add_ad navigation={this.props.navigation} />
                      ) : null}

              </Content>
              <Footer>
                      <FooterTab style={{backgroundColor:"#fff"}}>
                      <FooterTab style={{backgroundColor:"#fff"}}>
                          <Button  style={{backgroundColor:this.state.tab1 ? "#fff" : "#fff"}} vertical active={this.state.tab1} onPress={() => this.toggleTab1()}>
                          <Icon style={{color:this.state.tab1 ? "#998DFF" : "#000"}} active={this.state.tab1} type="MaterialCommunityIcons" name="home" />
                              <Text style={{fontSize:7,color:this.state.tab1 ? "#998DFF" : "#000"}}>Home</Text>
                          </Button>
                          <Button  style={{backgroundColor:this.state.tab2 ? "#fff" : "#fff"}}  vertical active={this.state.tab2} onPress={() => this.toggleTab2()}>
                          <Icon  style={{color:this.state.tab2 ? "#998DFF" : "#000"}}active={this.state.tab2} type="MaterialCommunityIcons" name="chat" size={18} />
                              <Text  style={{fontSize:7,color:this.state.tab2 ? "#998DFF" : "#000"}}>Messages</Text>
                          </Button>
                              {/* <Button style={{backgroundColor:this.state.tab3 ? "#fff" : "#fff"}} vertical active={this.state.tab3} onPress={() => this.toggleTab3()} >
                              <Icon style={{color:this.state.tab3? "#8cc43e" : "#000"}}  active={this.state.tab3} type="MaterialCommunityIcons" name="magnify" />
                                  <Text style={{fontSize:10,color:this.state.tab3 ? "#8cc43e" : "#000"}} >Search</Text>
                              </Button> */}

                        <TouchableOpacity vertical active={this.state.tab5} onPress={() => this.toggleTab5()} style={this.state.tab5?{paddingBottom:0,
                              height:80,marginTop:-20}:{height:80,paddingBottom:0,marginTop:-30}} >
                            <View style={{ borderRadius:40 , justifyContent:'center',alignItems:'center' ,backgroundColor:'#998DFF' ,padding:10 }}>
                            <Icon active={this.state.tab5} type="MaterialCommunityIcons" name="plus-circle"  style={{fontSize:30,color:'white'}}/>
                            </View>
                            </TouchableOpacity>

                              <Button style={{backgroundColor:this.state.tab3 ? "#fff" : "#fff"}} vertical active={this.state.tab3} onPress={() => this.toggleTab3()} >
                              <Icon style={{color:this.state.tab3 ? "#998DFF" : "#000"}} active={this.state.tab3} type="MaterialCommunityIcons" name="bell-ring" />
                              {   this.props.RX_items.length>0?<View style={[{ position:'absolute', top:-20,  paddingHorizontal :10
                            , paddingVertical:5, borderRadius :50 , } , !this.state.tab4 ?{ backgroundColor:'#998DFF'} : {backgroundColor:'#111'}]}>
                            <Text style={[{fontSize:7}, !this.state.tab3 ?{ color:'#111'} : {color:'#8cc43e'}]}>{this.props.RX_items.length}</Text>
                            </View>
                                :null}
                                  <Text style={{fontSize:5,color:this.state.tab3 ? "#998DFF" : "#000"}} >Notifications</Text>
                              </Button>
                              <Button style={{backgroundColor:this.state.tab5 ? "#fff" : "#fff"}} vertical active={this.state.tab5} onPress={() => this.toggleTab5()} >
                              <Icon style={{color:this.state.tab5 ? "#998DFF" : "#000"}}  active={this.state.tab5} type="MaterialCommunityIcons" name="account-outline" />
                                  <Text style={{fontSize:7,color:this.state.tab5 ? "#998DFF" : "#000"}} >Me</Text>
                              </Button>
                      </FooterTab>
                      </FooterTab>
              </Footer>
      </Container>

        )
  }
}

const mapStateToProps = state => {
    return {
        RX_items: state.cart.items,
        RX_selectedItem: state.cart.selectedItem,

    
    };
}
const mapDispatchToProps = dispatch => {
    return {
        OnAdd: item => { dispatch(addItem(item)) },
        OnRemove: item => { dispatch(deleteItem(item)) },
        OnSetAll: items => { dispatch(setItems(items)) },

    };
}


export default connect(mapStateToProps, mapDispatchToProps)(HomeTabs);