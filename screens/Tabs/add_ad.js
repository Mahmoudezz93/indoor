import React, { Component } from 'react';
import {
    Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
    ListItem, List, Left, Body, Thumbnail
  } from 'native-base';
  import StoreMan from "../../functions/storage";
  import axios from "axios";
  import { Image, Linking, Dimensions, ScrollView, FlatList, Clipboard } from "react-native";
  import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
  import Loader from "../loader;
  import { API, APIV, Address } from "../../functions/config";

  const deviceHeight = Dimensions.get("window").height;
  const deviceWidth = Dimensions.get("window").width;
  import { Keyboard } from 'react-native'
  import Community from"../../components/CommunityPicker";

export default class addAddress extends Component {

    
  constructor(props) {
    super(props);
    this.Storager = new StoreMan();
  };


  
  state = {
      
    token: null, 
    data: [], isReady: false,

    first_name:'',
    last_name:'',
    city_id:'',
    street:'',
    governrate_id:'',
    landline:'',
    shipping_note:'',
    apartment:'',
     building:'',
    phone:'',
    floor:'',
    landmark:'',
     
    editMode: false, addCollection: false,
    fetching_from_server: false, search_text: "",
    endThreshold: 2, text: '', page: 1,
    listLength: null, isModalVisible: false, address_name: "", refreshing: false
  }


  componentDidMount = async () => {
    await this.setState({ isReady: false });
    const stored_token = await this.Storager.get_item("USER_TOKEN");
    await this.setState({token:stored_token});
   // await this.Get_Collections();    
    ;
    await this.setState({ isReady: true });
   }


  //  AddAddress = async () => {
  //   var self = this;
  //   self.setState({ isReady: false });
  //   let token = this.state.token;
  //   //here you are login request 
  //    await axios
  //     .post(API + APIV + Address, {
  //        headers: {
  //         "Content-Type": "application/json",
  //         "Accept": "application/json",
  //         "Authorization": " Bearer " +  token
  //       },

  //       city_id: this.state.city_id,
  //       first_name: this.state.first_name,
  //       last_name: this.state.last_name,
        
  //       street:this.state.street,
  //       building:this.state.building,
  //       floor:this.state.floor,

  //       landmark:this.state.landmark,
  //       landline:this.state.landline,

  //       shipping_note:this.state.shipping_note,
  //       apartment:this.state.apartment,
  //     })
  //     .then(result => {
  //       console.log("address has been added");
         
  //       this.setState({
  //         city_id: '',
  //         governrate_id:'',
  //         first_name: '',
  //         last_name: '',
        
  //         street: '',
  //         building: '',
  //         floor: '',

  //         landmark: '',
  //         landline: '',

  //         shipping_note: '',
  //         apartment: ''
  //       })
  //     })
  //     .catch(function (error) {
  //       if (error.response) {
  //         //error handling
  //         alert(error.response.data.error);
  //       }
  //     });
  //   this.setState({ isReady: true });

  // }








  AddAddress = async () => {
    var self = this;
    self.setState({ isReady: false });
    let token = this.state.token;
    //here you are login request 
   var config = { headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": " Bearer " +  token
    }}

    var bodyParameters = {
      city_id: this.state.city_id,
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      
      street:this.state.street,
      building:this.state.building,
      floor:this.state.floor,

      landmark:this.state.landmark,
      landline:this.state.landline,

      shipping_note:this.state.shipping_note,
      apartment:this.state.apartment,
    }

     await axios
      .post(API + APIV + Address, bodyParameters, config   )
      .then(result => {
        alert("Address Added Successfully");
        console.log("address has been added");
         
        this.setState({
          city_id: '',
          governrate_id:'',
          first_name: '',
          last_name: '',
        
          street: '',
          building: '',
          floor: '',

          landmark: '',
          landline: '',

          shipping_note: '',
          apartment: '',
        })
      })
      .catch(function (error) {
        if (error.response) {
          //error handling
          alert(error.response.data.error);
        }
      });
    this.setState({ isReady: true });

  }



















    render()  {
        let data = this.state.data;
        return (
          this.state.isReady ?
            <Container>
               <Header style={{ backgroundColor: '#8CC43E' }} androidStatusBarColor={'#fff'} iosBarStyle={Platform.OS=='ios'?'':'light-content'} >
                <View padder style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: "center" }}>
                  <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                    <Icon onPress={() => this.props.navigation.goBack()} style={{ color: "#fff", paddingRight: 10, }} type="FontAwesome" name="angle-left" size={24} />
                  </TouchableOpacity>
                  <Text numberOfLines={2} ellipsizeMode='head' style={[ {
                    paddingHorizontal: 0.5, flex: 1
                    ,   fontSize: 16, color: "#fff"
                  }]}>Add Address</Text>
     
                </View>
    
              </Header>
              <ScrollView  showsVerticalScrollIndicator={false} style={{ width:deviceWidth-10,paddingHorizontal:5 ,backgroundColor:'#fff'}}>
              <View style={{   justifyContent:'space-between',flexDirection:'row', width: deviceWidth - 20,
               alignSelf: "center", alignItems: "space-around",paddingVertical :20 }}>
                    
                      <Item stackedLabel>
                        <Label style={[  { fontSize:12,width:160}]}>First Name *
                   </Label>
                        <Input
                          style={{ marginTop: 0, fontSize:15  }}
                          placeholder=" first name"
                          placeholderTextColor={"#707070"}
                          onChangeText={text => this.setState({first_name:text})}
                          value={this.state.first_name}
                          defaultValue={this.state.first_name}
                          
                        />
                      </Item>
                      <Item stackedLabel>
                        <Label style={[  {  fontSize:12,width:160 }]}>Last Name *
                </Label>
                <Input
                          style={{ marginTop: 0, fontSize:15  }}
                          placeholder=" first name"
                          placeholderTextColor={"#707070"}
                          onChangeText={text => this.setState({last_name:text})}
                          value={this.state.last_name}
                          defaultValue={this.state.last_name}
                          
                        />
                      </Item>
                    </View>

                    
                    <Item stackedLabel>
                        <Label style={[  { fontSize:12,width:deviceWidth-20,marginTop:12}]}>Landline 
                   </Label>
                   <Input
                          style={{ marginTop: 0, fontSize:15  }}
                          placeholder="landline"
                            keyboardType="phone-pad"
                           placeholderTextColor={"#707070"}
                     onChangeText={text => this.setState({landline:parseInt(text)})}
                value={this.state.phone?(`${this.state.landline}`):null}
                defaultValue={this.state.landline?(`${this.state.landline}`):null}  
                          
                        />
                      </Item> 


                    <Item stackedLabel>
                        <Label style={[  { fontSize:12,width:deviceWidth-20,marginTop:12}]}>Address Details *
                   </Label>
                        
                      </Item>
               


               
                    <View style={{ width: deviceWidth - 20 }}>
                            <Govrn cityChange={this.handleCity} governChange={this.handleGovrn} />
                    </View>

                    <Item stackedLabel>
                        <Label style={[  { fontSize:12,width:deviceWidth-20,marginTop:12}]}>Street *
                   </Label>
                   <Input
                          style={{ marginTop: 0, fontSize:15  }}
                          placeholder=" street"
                          placeholderTextColor={"#707070"}
                          onChangeText={text => this.setState({street:text})}
                          value={this.state.street}
                          defaultValue={this.state.street}
                          
                        />
                      </Item>


                      <Item stackedLabel>
                        <Label style={[  { fontSize:12,width:deviceWidth-20,marginTop:12}]}>Landmark 
                   </Label>
                   <Input
                          style={{ marginTop: 0, fontSize:15  }}
                          placeholder=" landmark"
                          placeholderTextColor={"#707070"}
                          onChangeText={text => this.setState({landmark:text})}
                          value={this.state.landmark}
                          defaultValue={this.state.landmark}
                          
                        />
                      </Item>



                      <Item stackedLabel>
                        <Label style={[  { fontSize:12,width:deviceWidth-20,marginTop:12}]}>Building *
                   </Label>
                   <Input
                          style={{ marginTop: 0, fontSize:15  }}
                          placeholder=" building"
                          placeholderTextColor={"#707070"}
                          onChangeText={text => this.setState({building:text})}
                          value={this.state.building}
                          defaultValue={this.state.building}
                          
                        />
                      </Item>

                      <Item stackedLabel>
                        <Label style={[  { fontSize:12,width:deviceWidth-20,marginTop:12}]}>Floor 
                   </Label>
                   <Input
                          style={{ marginTop: 0, fontSize:15  }}
                          placeholder=" floor"
                          placeholderTextColor={"#707070"}
                          onChangeText={text => this.setState({floor:text})}
                          value={this.state.floor}
                          defaultValue={this.state.floor}
                          
                        />
                      </Item>
                      <Item stackedLabel>
                        <Label style={[  { fontSize:12,width:deviceWidth-20,marginTop:12}]}>Apartment 
                   </Label>
                   <Input
                          style={{ marginTop: 0, fontSize:15  }}
                          placeholder=" apartment"
                          placeholderTextColor={"#707070"}
                          onChangeText={text => this.setState({apartment:text})}
                          value={this.state.apartment}
                          defaultValue={this.state.apartment}
                          
                        />
                      </Item>


                     
                      <Item stackedLabel>
                        <Label style={[  { fontSize:12,width:deviceWidth-20,marginTop:12}]}>Shipping note 
                   </Label>
                   <Input
                          style={{ marginTop: 0, fontSize:15  }}
                          placeholder=" shipping note"
                          placeholderTextColor={"#707070"}
                          onChangeText={text => this.setState({shipping_note:text})}
                          value={this.state.shipping_note}
                          defaultValue={this.state.shipping_note}
                          
                        />
                      </Item>





                      <Button onPress={() => { this.AddAddress(); }} rounded style={{width:280,height:50,borderRadius:13,marginVertical:30,backgroundColor:'#8cc43e',alignContent:'center',alignItems:'center',alignSelf:'center',justifyContent:"center"}} >
                                            <Text style={{color:'#fff',fontSize:18}} >Save Address</Text></Button>
               
              </ScrollView>
    
             
    
    
    
               
    
            </Container>
            : <Loader />
        )
      }

      validate_UNa = (text, type) => {
        if (type == 'firstname') {
          var self = this;
          self.setState({
            first_name: text
          });
          if (text.length < 2) {
            self.setState({ FirstNameError: true });
          }
          else {
            self.setState({ FirstNameError: false });
    
          }
        }
        if (type == 'lastname') {
          var self = this;
          self.setState({
            last_name: text
          });
          if (text.length < 2) {
            self.setState({ LastNameError: true });
          }
          else {
            self.setState({ LastNameError: false });
    
          }
        }
        if (type == 'address') {
          var self = this;
          self.setState({
            address: text
          });
          if (text.length < 3) {
            self.setState({ AddressError: true });
          }
          else {
            self.setState({ AddressError: false });
    
          }
        }
      }

        //handlers
  handleCity = async (data) => {
    await this.setState({ city_id: data });
    //console.log(this.state.city_id);
  }
  handleGovrn = async (data) => {
    await this.setState({ governrate_id: data });

  }

}
