import React, { createRef, Component } from 'react';


import {
    Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
    ListItem, List, Left, Body, Thumbnail
} from 'native-base';
import StoreMan from "../../functions/storage";
import { API, APIV, Posts } from "../../functions/config";

import axios from "axios";
import { Image, Linking, Dimensions, ScrollView, FlatList, Clipboard, TextInput } from "react-native";
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import Loader from "../loader";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import { Keyboard } from 'react-native'
import styles from "../style"
import SmallLoader from "../smallLoader";

import { Rating, AirbnbRating } from 'react-native-ratings';
//const Rating_img = require('../../assets/ic_rating.png');





export default class CategoriesPage extends Component {

    constructor(props) {
        super(props);
        this.Storager = new StoreMan();
    };



    state = {
        token: null, data: [], isReady: false, editMode: false ,
      name:'', sub_category_id:'', sortBy:'',
      sortDir:'',
      brand:'', min_price:0, max_price:1000000,
      page: 1,
      fetching_from_server: false, search_text: "",focusOnSearch:false,
      endThreshold: 2, text: '',group_id:'1',
      refreshing: false, 
      
    }

    componentDidMount = async () => {
        await this.setState({ isReady: false });
        const stored_token = await this.Storager.get_item("USER_TOKEN");
        await this.setState({ token: stored_token });
        const { navigation } = this.props;
        const Data = navigation.getParam('data', '');
        let focusOnSearch = navigation.getParam('focus', false);
        await this.setState({ category: Data })
        await this.setState({ focusOnSearch: focusOnSearch })

        await this.Get_Products();
        await this.setState({ isReady: true });
    }



    Get_Products = async (mode) => {
        this.setState({ fetching_from_server: true });
        if (mode == 'reset') {
            await axios
 

                .get(API + APIV + Posts + "?page=1"  + "&sub_category_id=" + this.state.sub_category_id
                    + "&sortBy=" + this.state.sortBy + "&sortDir=" + this.state.sortDir 
                    + "&min_price=" + this.state.min_price + "&max_price=" + this.state.max_price + "&group_id=" + this.state.group_id

                    , {
                        headers: {
                           "Content-Type": "application/json",
                        "Accept": "application/json",
                        "Authorization": " Bearer " + this.state.token
                        },
                    })
                .then(result => {
                    this.setState({
                        data: result.data.data.posts,
                        page: 2 //next page

                    })
                })
                .catch(error => {
                    console.log(error);
                    if (error.response) {
                        // error handling
                        alert(error.response.data.error);
                    }
                });
        } else {
            console.log('get data of page -> ' + this.state.page)
            await axios
                .get(API + APIV + Posts + "?page=" + this.state.page + "&sub_category_id=" + this.state.sub_category_id
                + "&sortBy=" + this.state.sortBy + "&sortDir=" + this.state.sortDir 
                + "&min_price=" + this.state.min_price + "&max_price=" + this.state.max_price + "&group_id=" + this.state.group_id 
                , {
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        "Authorization": " Bearer " + this.state.token
                    },

                })
                .then(result => {
                    console.log("iamhere")
                    if (result.data.data.posts.length > 0) {
                        this.setState({ data: [...this.state.data, ...result.data.data.posts] })
                        this.setState({ page: this.state.page + 1 })

                    } else {
                        console.log("endreached")
                    }
                })
                .catch(error => {
                    console.log(error.response.data.error);
                });


        }
        // console.log(this.state.data);

        this.setState({ fetching_from_server: false });
        console.log(this.state.data)
    }




    _onRefresh = () => {
        this.setState({ refreshing: true });
        this.Get_Products('reset').then(() => {
            this.setState({ refreshing: false });
        });
    }


    setSearchText = (text) => {
        var self = this;
        self.setState({ name: text })
    }





    render() {
        let data = this.state.data;
        return (
            this.state.isReady ?
                <Container>
                    <View padder style={{ backgroundColor: '#998DFF', height: 100 }} androidStatusBarColor={'#fff'} iosBarStyle={Platform.OS == 'ios' ? '' : 'light-content'} >


                        <Text ellipsizeMode='head' style={[{
                            paddingHorizontal: 0.5, flex: 1
                            , fontSize: 16, color: "#fff"
                        }]}>Welcome   :) </Text>
                        <Text ellipsizeMode='head' style={[{
                            paddingHorizontal: 0.5, flex: 1
                            , fontSize: 16, color: "#fff"
                        }]}>Community Name</Text>



                    </View>
                    <View style={{ flex: 1  }}>
                    

 
             
             
                      
    
    
     
    
              <View style={{ flex: 1,borderTopWidth:1,borderTopColor:'#cdcdcd',paddingBottom:30,backgroundColor:'#F6F6F6' }}>
                {this.state.data.length > 0 ?  
                          <View>   
                          
                
                <FlatList 
                  style={{
                    width: deviceWidth, height: null,  alignContent: 'center', paddingTop: 1.5
                    , paddingBottom: 10 ,paddingHorizontal:1,marginTop:5,
                  }}
                  data={this.state.data}
                   onEndReachedThreshold={this.state.endThreshold}
                  onEndReached={() => {
                    this.state.fetching_from_server == false ? this.Get_Products('scroll') : null
                  }}
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh.bind(this)}
    
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({ item, index, separators }) => {
                    let value = item;
    
                    return (
    
                        <ListItem key={index} onPress={() => this.props.navigation.navigate("StoreTabs", { data: value.id })}>
                        <View style={{ alignSelf: 'center' ,borderRadius:15 ,marginRight:10}}>
                          <Image style={{ width: 60, height: 60, resizeMode: 'cover' ,borderRadius:5 ,backgroundColor:'#998DFF' }}
                            source={{ uri: value.images.image }}
                          />
                        </View>
                        <View style={{flex: 1, alignSelf: 'center', alignItems: 'flex-start', flexDirection: 'column'}}>
                        <View style={{padding:10 ,borderRadius:10 ,position:"absolute" ,right:0}}>
                              <Text style={[styles.TitleSmallWhite]}>{value.price} EGP</Text>
                            </View>
                          <View style={{ flex:1,alignSelf: 'flex-start', flexDirection: 'row', justifyContent: 'space-between', }}>
                            <Text style={[styles.TextBrandTitle],{flex:0.75, color:"#000" ,fontWeight:"bold"}}>{value.title}Title</Text>
                          </View>
                          <View style={{ alignItems:"center", justifyContent:'flex-start',alignContent:'flex-start',alignItems:'flex-start',marginVertical:10}}>
                           <Text numberOfLines={5} style={{textAlign:'left' }}>{value.description}</Text>

                          </View>
                          <Text note numberOfLines={1}>{value.address}</Text>
                        </View>
                      </ListItem>
                    )
                  }} />
                  </View>
                  : 
                  <View style={{ alignContent: "center", alignSelf: "center", justifyContent: "center", flex: 1, padding: 20 }}>
                      <Text style={styles.TextBrand}> There is no data available ! 😴</Text>
                    </View>}
    
              </View>
              {this.state.fetching_from_server ? (
                  <SmallLoader/>
                ) : null}
              </View>

 

                </Container>
                : <Loader />
        )
    }
}
