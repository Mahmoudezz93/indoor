import React, { Component } from 'react';
import {
    Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
    ListItem, List, Left,
    Body,
    Thumbnail
  } from 'native-base';
import { connect } from "react-redux";
import { addItem, deleteItem, setItems, addWish, deleteWish, setWishes } from "../store/actions/index"
import { TouchableOpacity } from 'react-native';

class welcomePage extends Component {

    constructor(props) {
        super(props);
        //this.Storager = new StorageManager();

    }

    state = { data: [], isReady: false, cartProducts: [] }

    componentDidMount = async () => {
        this.setState({ isReady: false });
        //const token = await this.Storager.get_item("USER_TOKEN");
        await this.setState({ cartProducts: this.props.RX_items });
        //await this.calculate_price();
        this.setState({ isReady: true });
    }



    AddToCart = async (itemType) => {
        let product = this.state.data;
        const newItem = {
            id: Math.random(),  //
            name: "productName",  // 
            description: product.description,
            image: product.image,
            qty: 1,
            note: "nothing",
            price: product.price,
            discount: product.discount,
            rate: product.rate,
            class: itemType,

        }
        await this.props.OnAdd(newItem);
        console.log(this.props.RX_items);

    }


    RemoveFromCart = async (value) => {
        await this.props.OnRemove(value);
        await this.setState({ products: this.props.RX_items });
       }


       AddToWishList = async (itemType) => {
        let product = this.state.data;
        const newItem = {
            id: Math.random(),  //
            name: "productName",  // 
            description: product.description,
            image: product.image,
            qty: 1,
            note: "nothing",
            price: product.price,
            discount: product.discount,
            rate: product.rate,
            class: itemType,

        }
        await this.props.OnAddWish(newItem);
        console.log(this.props.RX_items);  

    }

    RemoveFromWishList = async (value) => {
        await this.props.OnRemoveWish(value);
        await this.setState({ products: this.props.RX_wishItems });
       }


    render() {
        return (
            <View padder style={{ flex:1}}>
                <Text style={{ alignContent: 'center', textDecorationLine: 'underline', fontWeight: 'bold' }}>Cart Section </Text>
                <TouchableOpacity onPress={() => this.AddToCart(1)}>
                    <Text style={{ color: 'blue' }}>Add a product</Text>
                </TouchableOpacity>

                <Text>
                    number of products in store {this.props.RX_items.length}
                </Text>
                <Content style={{ height:100,   borderBottomWidth: 0.5, borderColor: '#CDCDCD' }} >                
                 {this.props.RX_items.length > 0 ?

                    this.props.RX_items.map((value, index) => {
                        let product = value
                        return (
                            <List
                                style={{ marginBottom: 10, backgroundColor: '#fff',height:null}}
                                key={index}>
                                <ListItem >
                                    <Left style={{ flex: 0.1 }}>
                                        <Thumbnail large resizeMethod='auto' resizeMode="contain"
                                            source={{ uri: product.image }} >
                                        </Thumbnail>
                                    </Left >
                                    <Body style={{ flex: 0.8 }}>
                                        <View >
                                            <Text numberOfLines={2} style={[ ], { width: "95%", fontWeight: 'bold' }}>{product.name}</Text>
                                            <Text numberOfLines={2} note> </Text>
                                        </View>
                                    </Body>
                                    
                                </ListItem>
                                <ListItem style={{ justifyContent: "space-between", paddingHorizontal: 10 }}>
                                    <Text><Text note >Price: </Text> </Text>
                                    <Text><Text note >Quantity: </Text> </Text>
                                </ListItem>
                                <ListItem onPress={() => this.RemoveFromCart(value)} style={{ justifyContent: "center", paddingHorizontal: 10 }}>
                                    <Text note>Remove this item </Text>
                                </ListItem>


                            </List>

                        );

                    }
                    ) :
                    <View style={{ alignContent: "center", alignSelf: "center", justifyContent: "center", flex: 1, padding: 20 }}>
                        <Text  > Your Cart is Empty ! 😴</Text>
                    </View>
                }

</Content>  



<Text>
                    number of products in store {this.props.RX_items.length}
                </Text>


               

                <Text style={{ alignContent: 'center',borderTopColor:"black",borderTopWidth:10,paddingVertical:10,marginTop:20, textDecorationLine: 'underline', fontWeight: 'bold' }}>Cart Section </Text>
                <TouchableOpacity onPress={() => this.AddToWishList(1)}>
                    <Text style={{ color: 'blue' }}>Add a product</Text>
                </TouchableOpacity>

                <Text>
                    number of products in the wishlist is  {this.props.RX_wishItems}
                </Text>

            </View>
        )
    }
}


const mapStateToProps = state => {
    return {
        RX_items: state.cart.items,
        RX_selectedItem: state.cart.selectedItem,

    
    };
}
const mapDispatchToProps = dispatch => {
    return {
        OnAdd: item => { dispatch(addItem(item)) },
        OnRemove: item => { dispatch(deleteItem(item)) },
        OnSetAll: items => { dispatch(setItems(items)) },

    };
}


export default connect(mapStateToProps, mapDispatchToProps)(welcomePage);