const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const basefont = 16;
export default {

    ImageContainer: {
        backgroundColor: "red",
        height: deviceHeight / 1.2,
        width: deviceWidth
    },
    Card: {
        width: deviceWidth - 20,
        height: deviceHeight / 3,
        backgroundColor: "black",
        alignSelf: "center",
        marginVertical: 20,
        shadowColor: "#000",
        borderRadius:10,
        shadowOffset: {
            width: 0,
            height: 1.5,
        },
        shadowOpacity: 0.5,
        shadowRadius: 2.22,
        elevation: 3,
    },
    ImageCard: {
        resizeMode: "cover",
        height: deviceHeight / 3.5,
        backgroundColor:"white"
    },
    FooterCard:{
        backgroundColor:"#FF952B",
        
        alignItems:"center",
        justifyContent:"center",
        height:deviceHeight/3 -deviceHeight/3.5
    },
    TextSmallWhite:{
        color:'white',
        fontWeight:'bold',
        textTransform:'uppercase'
    },
    TextList:{
        fontWeight:'bold',
        fontSize:16
    },
    ImageHeader:{
        width:deviceWidth,
        height:300,
        resizeMode:"contain",
    },
    InfoList:{
        alignItems:"center",
        flexDirection:"row"
    },
    Line:{
    },
    TextSmalBlack:{
        color:'#111',
    },
    TextSmalBlackBold:{
        color:'#111',
        fontWeight:'bold',
    },
    TextSmalGreyBold:{
        color:'#444',
        fontWeight:'bold',
    },
    TextSmalGrey:{
        color:'#888',
    },
    CartButton:{ 
        width: deviceWidth/1.3 , height:50, backgroundColor:"#FF952B" ,
        borderRadius:50 , alignSelf:"center"
       ,paddingHorizontal:10,justifyContent:"center" ,alignItems:"center"
    },
    CardButtonText:{
        color:'white',
        fontWeight:'bold',
    },
    Offerbadge:{position:'absolute' ,borderBottomLeftRadius:5, right :0 ,top:1 ,
    paddingLeft:15,paddingRight:10,
    paddingVertical:10,
    backgroundColor:'#FF952B'},


    
    yellowtab: { backgroundColor: '#F9E159', height: 40, width: 260, borderRadius: 10, flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', alignContent: 'center', margin: 1},
    profilecard: { width: deviceWidth / 1.1, minHeight: 180, alignSelf: 'center', marginHorizontal: 2, marginVertical: 5, backgroundColor: 'white', flexDirection: 'column' },
    profilecardWhite:{width: deviceWidth / 1.1, height: 300, alignSelf: 'center', marginHorizontal: 2, marginVertical: 5, backgroundColor: 'white', flexDirection: 'column',alignContent: 'center',borderWidth:1,borderColor:'black' },
    profileCardRed:{width: deviceWidth / 1.1, height: 220, alignSelf: 'center', marginHorizontal: 2, marginVertical: 5, backgroundColor: '#D42323', flexDirection: 'column',alignContent: 'center',padding:12},
    inTabTextWhite:{paddingHorizontal: 0.9, flex: 1, fontSize: 15, color: "white", textAlign: 'center'},
    avatar: { backgroundColor: 'blue', height: 120, width: 120, alignSelf: 'center', marginVertical: 10, borderRadius: 60 },
    inTabText: { paddingHorizontal: 0.9, flex: 1, fontSize: 15, color: "black", textAlign: 'center' },
    inCardText: { paddingHorizontal: 0.9, flex: 1, fontSize: 13, color: "black", textAlign: 'center' },
    buttonCard: { backgroundColor: '#F0EEEE', width: deviceWidth / 1.1, height: deviceHeight / 4.5, alignSelf: 'center', marginHorizontal: 2, marginVertical: 5, flexDirection: 'column' },
    GreyCard: { backgroundColor: '#F0EEEE', width: deviceWidth / 1.1, height: null, alignSelf: 'center', justifyContent: 'space-around', marginHorizontal: 2, marginVertical: 5, flexDirection: 'row', padding: 10, },
    profileTabs:{backgroundColor: 'white', width: deviceWidth / 1.1, height: null, alignSelf: 'center', marginHorizontal: 2, marginVertical: 5, flexDirection: 'row', paddingHorizontal: 10,marginVertical: 15},
    profileTabsText:{ paddingHorizontal: 10, flex: 1, fontSize: 15, color: "black" },
    fqaTab:{backgroundColor:'#F0EEEE',color:'black',fontSize:13,minHeight:50,textAlign:"justify",borderColor:'black',borderWidth:3,borderTopLeftRadius:10,borderTopRightRadius:10,borderBottomWidth:0,padding:3},
    fqaTaba:{backgroundColor:'white',color:'black',fontSize:13,borderRadius:15,textAlign:'center',borderBottomLeftRadius:10,borderBottomRightRadius:10,borderWidth:3,borderTopWidth:0,marginBottom:5,padding:3, borderColor:'black'},
    redWideTab:{backgroundColor: '#D42323', height: 40, width: 320, borderRadius: 10, flexDirection: 'row', alignSelf: 'center' ,padding:5, marginVertical: 15},

    fullProfileCard:{width:deviceWidth/1.1,height:null,alignSelf:'center',borderWidth:1,borderColor:'#F0EEEE',padding:10,marginVertical:10,borderRadius:10},
    profileInfoTab:{width:deviceWidth/1.23,minHeight:40,borderColor:"#F0EEEE",borderBottomWidth:1,alignSelf:'center',padding:4,paddingHorizontal:3},

    reviewsCard:{borderColor:'red',borderWidth:1,width:deviceWidth,backgroundColor:'white',alignSelf:'center',alignItems:'flex-start',alignContent:'flex-start',flexDirection:'column'},
    

}