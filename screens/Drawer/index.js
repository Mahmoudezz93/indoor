import React, { Component } from "react";
import { Image, Linking, Dimensions, ScrollView, FlatList, Clipboard } from "react-native";    import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import {
  Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
  ListItem, List, Left, Body, Thumbnail,Title,
} from 'native-base';
 
import axios from "axios";
import { API, APIV, Categories,GetProfile  } from "../../functions/config";

import StoreMan from "../../functions/storage";
import { createDrawerNavigator ,DrawerItems} from 'react-navigation-drawer'
 
   const deviceHeight = Dimensions.get("window").height;
  const deviceWidth = Dimensions.get("window").width;
  import { Keyboard } from 'react-native'
  import Loader from "../../screens/loader"
 import CategoriesPage from "../ecommerce/categories"
 import { SafeAreaView } from "react-navigation";
 
import styles from "../../screens/style"
class SideBar extends Component {
  

  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4
    };
    this.Storager = new StoreMan();

  }
  state = { email:null, data:[]  ,page: 1,
    fetching_from_server: false, search_text: "",token:'',isReady:false,
    endThreshold: 2,}

  componentDidMount = async () => {
    await this.setState({ isReady: false });
    const stored_token = await this.Storager.get_item("USER_TOKEN");
    await this.setState({token:stored_token});
    await this.Get_Profile();
    await this.setState({ isReady: true });

   }

  flush = async () => {
    await this.Storager.remove_all();
    this.props.navigation.replace("Splash")
  }
  
  
   
  
  
  Get_Profile = async () => {
    await axios
      .get(API + APIV + GetProfile, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": " Bearer " + this.state.token
        },
      })
      .then(result => {
         this.setState({
          data: result.data.data.profile,
          
        })
      })
      .catch(function (error) {
        console.log(error);
        if (error.response) {
          //error handling
          console.log(error.response.data.error);
        }
      });
      console.log(this.state.data)
  }
  
  
  
  
  
  
  render() {
    return (
      this.state.isReady ?
      <Container style={{ backgroundColor: "white" }} >
        <Content
          bounces={false}
          style={{ flex: 1, top: -1, flexDirection:'column'}}
        >
          <View style={{
            flexDirection: "row", flex: 1, justifyContent: "space-between",
            padding: 10 ,backgroundColor: '#998DFF'  
          }}>
 
            <Title style={{ color: "white" }} > InDoor App</Title>
            <Icon type="MaterialCommunityIcons" name="window-close" 
             size={32} onPress={() => this.props.navigation.closeDrawer()} style={{color:'white'}}/>
          </View>

          <View style = {[styles.profilecard,{backgroundColor:'#f6f6f6',marginTop:0,paddingVertical:10}]}>
            <Image style={[styles.avatar,{backgroundColor:'#ededed',borderWidth:3,borderColor:'#998DFF'}]} 
            source={{ uri: this.state.data ? this.state.data.avatar : null  }} />
             
            <Text style={{fontSize:18,color:'black',alignSelf:'center',marginVertical:5}}>{this.state.data ? this.state.data.name : 'Welcome'}</Text>
            <Text
            style={{fontSize:18,color:'black',alignSelf:'center',marginVertical:5}}
            onPress={() => { this.props.navigation.navigate("Login") }} >{! this.state.data ? 'Login / Signup' : null }</Text>
             
        </View>
   
          
  

      <View style={{}}>
            <ListItem style={{ alignItems: "center" }} onPress={() => this.props.navigation.navigate("HomeTabs")} >
            <Text  ><Icon name="home-outline" type="MaterialCommunityIcons" style={{fontSize:18}}/>  Home</Text>
          </ListItem>

            {/* <ListItem style={{ alignItems: "center" }} onPress={() => this.props.navigation.navigate("Categories")} >
            <Text  ><Icon name="bell-outline" type="MaterialCommunityIcons" style={{fontSize:18}}/>  Categories</Text>
          </ListItem> */}
            <ListItem style={{ alignItems: "center" }} onPress={() => this.props.navigation.navigate("Profile")} >
            <Text  ><Icon name="user" type="Feather" style={{fontSize:18}} />  Me</Text>
          </ListItem>
          <ListItem style={{  alignItems: "center" }} onPress={() => this.flush()} >
          <Text><Icon name="power" type="MaterialCommunityIcons" style={{ fontSize:18 }} />  Logout</Text>
          </ListItem>

          </View>
        </Content> 
      </Container>
      : <Loader />)
  }
}

export default SideBar;
