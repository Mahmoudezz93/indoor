import React, { Component } from 'react';
import { Image,  Dimensions, ScrollView, TouchableOpacity, FlatList } from "react-native";

import {
    Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
    ListItem, List, Left, Body, Thumbnail
  } from 'native-base';
  const deviceHeight = Dimensions.get("window").height;
  const deviceWidth = Dimensions.get("window").width;
  import { StackActions ,NavigationActions} from 'react-navigation';

  import axios from "axios";
  import { API, APIV, Login, } from "../../functions/config";
  import StoreMan from "../../functions/storage";
  import Loader from '../loader';
  import { Alert } from "react-native";

  export default class login extends Component {


    constructor(props) {
        super(props);
        this.Storager=new StoreMan();
      };
      state = {
        email: "",
        password: "",

        isReady: true
      }
    
      Login = async () => { 
        var self=this;
        this.setState({ isReady: false });
        //here you are login request 
        await axios
          .post(   API + APIV + Login, {
            headers: {
              "Content-Type": "application/json",
              "Accept": "application/json"
            },
            email: this.state.email,
            password: this.state.password
          })
          .then(result => {
            console.log("User has been set");
            self.saveData(result);
    
          })
          .catch(function (error) {
            console.log(error);
            if (error.response) {
              //error handling
              Alert.alert(error.response.data.error);
            }
          });
        this.setState({ isReady: true });
    
      }
    saveData=(result)=>{
      var self=this;
       this.Storager.set_item("USER_TOKEN",result.data.data.user.token);
      // this.Storager.set_item("USER_ID",result.data.data.client.id.toString());
       this.Storager.set_item("USER_AVATAR",result.data.data.client.avatar);
       console.log("login done")
       this.navigateToLogin();
    
    }
    navigateToLogin() {
      this.props.navigation.dispatch(
        StackActions.reset({
        index: 0,
        key: null,
        actions: [NavigationActions.navigate({ routeName: 'Drawer' })]
        })
        )
      this.props.navigation.dispatch(resetAction);
    }
    render() {
        return (
             
                this.state.isReady ?
    
    
                    <Container>
    
                             <Content style={{backgroundColor:'#998DFF'}}>
    
                                <View style={{   flexDirection: 'column', width: deviceWidth, height: deviceHeight, alignItems: 'center',justifyContent:'center',alignContent:'center' }}>
      

                                    <Form style={{ width:deviceWidth,height:null, paddingHorizontal: 10 }}>
                                        <View style={{ width: 200,height:null, alignSelf: "center", justifyContent: "space-around" }}>
                                            <Item floatingLabel  >
                                                <Input
                                                    placeholder={'Email'}
                                                    placeholderTextColor="white"
                                                    
                                                    style={{ color: 'white', alignSelf: 'center',textAlign:'left' }}
                                                    onChangeText={text => this.validate_EM(text, "Email")}
                                                    value={this.state.email} />
                                            </Item>
                                            <Item floatingLabel
                                            >
                                                <Input
                                                    
                                                    secureTextEntry
                                                    placeholder={'Password'}
                                                    placeholderTextColor="white"
                                                    style={{ color: 'white', alignSelf: 'center',textAlign:'left'  }}
                                                    onChangeText={text => this.validate_PS(text, "password")}
                                                    value={this.state.password} />
                                            </Item>
                                        </View>
                                    </Form>
                                    
                                    <View style={{ height: null, width: deviceWidth/1.5,alignSelf:'center',alignItems:'center',justifyContent:'center',marginTop:30,padding:5 }}>
                                        <Button onPress={() => { this.Login(); }} rounded style={{width:150,height:null,borderRadius:14,backgroundColor:'white',alignContent:'center',alignItems:'center',alignSelf:'center',justifyContent:"center"}} >
                                            <Text style={{color:'#998DFF',fontSize:18}} >Login</Text></Button>
                                            <Text style={{color:'white',fontSize:10,marginVertical:10}} >Or</Text> 
     
                                        <TouchableOpacity style={{}} onPress={() => { this.props.navigation.navigate("SignUp") }}>
                                          <Text style = {{fontSize:14,color:'white'}}>need to<Text  style={{fontSize:12,color:'white',textDecorationLine:"underline",fontWeight:"bold",color:'#fff'}}> Register</Text></Text >
                                          </TouchableOpacity>
                                        <View padder style={{}}>
    
    
                                    <Text style={{fontSize:12,alignSelf:'center',textAlign:'center',color:'white',marginTop:60}}>By creating an account , you agree to our <Text style={{fontSize:13,color:"#fff",textDecorationLine:"underline"}} 
                                    onPress={() =>  Linking.openURL('https://takits.com/terms') }> Terms of Services </Text>and 
                                    <Text style={{fontSize:13,color:"#fff",textDecorationLine:"underline"}} 
                                    onPress={() => Linking.openURL('https://takits.com/policy')} > Privacy Policy </Text></Text>
                                    </View>
    
    
                                </View>
    
                                    </View>
                                   
                            </Content>
                     </Container>
                    : <Loader />
    
            )
    }

    validate_PS = (text, type) => {
        var self = this;
    
        if ((type = "password")) {
          self.setState({
            password: text
          });
          if (text.length < 5) {
            self.setState({ PasswordError: true });
          } else {
            self.setState({ PasswordError: false });
    
          }
        }
      };
    
      validate_EM = (text, type) => {
        var self = this;
    
        if (type == "Email") {
          self.setState({
            email: text
          });
          if (text.length < 5) {
            self.setState({ EmailError: true });
          } else {
            self.setState({ EmailError: false });
    
          }
        }
      }
}
