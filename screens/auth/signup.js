import React, { Component } from 'react';
import { Image,  Dimensions, ScrollView, TouchableOpacity, FlatList } from "react-native";

import {
    Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
    ListItem, List, Left, Body, Thumbnail
  } from 'native-base';
  const deviceHeight = Dimensions.get("window").height;
  const deviceWidth = Dimensions.get("window").width;
  import { StackActions ,NavigationActions} from 'react-navigation';

  import axios from "axios";
  import { API, APIV, Login,Points } from "../../functions/config";
  import StoreMan from "../../functions/storage";
  import Loader from '../loader';
  import Community from"../../components/CommunityPicker";

export default class welcomePage extends Component {
    state = {
        isReady: true,
        email: "",
        password: "",
        password_confirmation:'',
        
        token: "",
        name: "",
       
        phone:'',

         
        governrate_id:'',
        
        gender:"",
        currentPosition: 0,
        current: 0
    }

    constructor(props) {
        super(props);
        this.Storager = new StoreMan();
    }
    Sign = async () => {
        var self=this;
        this.setState({ isReady: false });
        //here you are login request 
        await axios
            .post(API + APIV + "auth/register", {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                name: this.state.name,
                email: this.state.email,
                password: this.state.password,
                password_confirmation: this.state.password_confirmation,
                phone: this.state.phone,
                group_id:this.state.governrate_id

            })
            .then(result => {
                console.log("User has been set");
                self.saveData(result);
            })
            .catch(function (error) {
                if (error.response) {
                    //error handling
                    alert(error.response.data.error);
                }
            });
        this.setState({ isReady: true });
    }

    navigateToLogin() {
        this.props.navigation.dispatch(
          StackActions.reset({
          index: 0,
          key: null,
          actions: [NavigationActions.navigate({ routeName: 'Drawer' })]
          })
          )}


          

    saveData=(result)=>{
        var self=this;
         this.Storager.set_item("USER_TOKEN",result.data.data.user.token);
        // this.Storager.set_item("USER_ID",result.data.data.client.id.toString());
         this.Storager.set_item("USER_AVATAR",result.data.data.client.avatar);
         this.props.navigation.replace("Drawer")
         //this.navigateToLogin();
      
      }



    render() {
        return (
             
                this.state.isReady ?
    
    
                    <Container>
    
                             <Content style={{backgroundColor:'#998DFF'}}>
    
                                <View style={{   flexDirection: 'column', width: deviceWidth, height: deviceHeight, alignItems: 'center',justifyContent:'center',alignContent:'center' }}>
     
                             

                                    <Form style={{ width:deviceWidth,height:null, paddingHorizontal: 10 }}>
                                        <View style={{ width: 300,height:null, alignSelf: "center", justifyContent: "space-around" }}>
                                        <Item floatingLabel  >
                                                <Input
                                                    placeholder={'User Name'}
                                                    placeholderTextColor="white"
                                                    
                                                    style={{ color: 'white', alignSelf: 'center',textAlign:'left' }}
                                                    onChangeText={text => this.setState({name:text})}
                                                    value={this.state.name} />
                                            </Item>

                                            <Item floatingLabel  >
                                                <Input
                                                    placeholder={'Email'}
                                                    placeholderTextColor="white"
                                                    
                                                    style={{ color: 'white', alignSelf: 'center',textAlign:'left' }}
                                                    onChangeText={text => this.setState({email:text})}
                                                    value={this.state.email} />
                                                    
                                            </Item>


                                            <Item floatingLabel
                                            >
                                                <Input
                                                    
                                                    secureTextEntry
                                                    placeholder={'Phone No.'}
                                                    keyboardType="phone-pad"
                                                    placeholderTextColor="white"
                                                    style={{ color: 'white', alignSelf: 'center',textAlign:'left'  }}
                                                    onChangeText={text => this.setState({phone:text})}
                                                    value={this.state.phone} />
                                            </Item>














                                            <Item floatingLabel
                                            >
                                                <Input
                                                    
                                                    secureTextEntry
                                                    placeholder={'Password'}
                                                    placeholderTextColor="white"
                                                    style={{ color: 'white', alignSelf: 'center',textAlign:'left'  }}
                                                    onChangeText={text => this.setState({password:text})}
                                                    value={this.state.password} />
                                            </Item>

                                            <Item floatingLabel
                                            >
                                                <Input
                                                    
                                                    secureTextEntry
                                                    placeholder={'Re-Password'}
                                                    placeholderTextColor="white"
                                                    style={{ color: 'white', alignSelf: 'center',textAlign:'left'  }}
                                                    onChangeText={text => this.setState({password_confirmation:text})}
                                                    value={this.state.password_confirmation} />
                                            </Item>


                                            <Community gov={''} 
                                                      city ={''}
                                                     cityChange= {this.handleCity} 
                                                      governChange={this.handleGovrn}   />
          

                                        </View>
                                    </Form>
                                    
                                    <View style={{ height: null, width: deviceWidth/1.5,alignSelf:'center',alignItems:'center',justifyContent:'center',marginTop:30,padding:5 }}>
                                        <Button onPress={() => { this.Sign(); }} rounded style={{width:150,height:null,borderRadius:14,backgroundColor:'white',alignContent:'center',alignItems:'center',alignSelf:'center',justifyContent:"center"}} >
                                            <Text style={{color:'#998DFF',fontSize:16}} >Register</Text></Button>
      
                                
                                        <View padder style={{}}>
    
    
                                    <Text style={{fontSize:12,alignSelf:'center',textAlign:'center',color:'white',marginTop:10}}>By creating an account , you agree to our <Text style={{fontSize:13,color:"#fff",textDecorationLine:"underline"}} 
                                    onPress={() =>  Linking.openURL('https://takits.com/terms') }> Terms of Services </Text>and 
                                    <Text style={{fontSize:13,color:"#fff",textDecorationLine:"underline"}} 
                                    onPress={() => Linking.openURL('https://takits.com/policy')} > Privacy Policy </Text></Text>
                                    </View>
    
    
                                </View>  
    
                                    </View>
                                   
                            </Content>
                     </Container>
                    : <Loader />
    
            )
    }
    
handleCity=async(data)=>
{
  data ? await this.setState({city_id:data})
  : await this.setState({city_id:this.state.data.city_id})
  //console.log(this.state.city_id);
}
handleGovrn=async(data)=>
{
  data ? await this.setState({governrate_id:data})
  : await this.setState({governrate_id:this.state.governrate_id});
console.log("the group id will be ",this.state.governrate_id)
}


}
