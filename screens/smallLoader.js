import React, { Component } from 'react'
import {
    Container, Header, Content, Form, Item, Input, Label, Icon, Text, Button, View, Right, Footer,
    ListItem, List, Left,
    Body,
    Thumbnail
} from 'native-base';
import { Image, Dimensions } from "react-native";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import {
    BarIndicator,
} from 'react-native-indicators';

export default class SmallLoader extends Component {
    render() {
        return (
            <View style={{height:30 }}>
<BarIndicator color={'#998DFF'} size={15}/>
</View>
)
    }
}