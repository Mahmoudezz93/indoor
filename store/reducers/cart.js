import {
  ADD_ITEM, DELETE_ITEM, SELECT_ITEM, DESELECT_ITEM, SET_CART_ITEMS, DELETE_WISH,
  SELECT_WISH, DESELECT_WISH, SET_CART_WISH, ADD_WISH
} from "../actions/actionTypes";
let initial_state = {
  items: [],
  selectedItem: null,
  
  wishItems: [],
  selectedWish: null,
}
const cart = (state = initial_state, action) => {

  switch (action.type) {

    /// cart // 
    case ADD_ITEM:
      if (state.items.find((value, index) => { return value.id === action.id }) != null) {
        state.items.filter((value, index) => { if (value.id === action.id) { return value.qty = action.qty + value.qty } else { return value.qty = value.qty } })
      }
      return {
        ...state,
        items: state.items.find((value, index) => { return value.id === action.id }) == null ?
          state.items.concat({
            id: action.id,
            description: action.description,
            qty: action.qty,
            image: action.image,
            name: action.name,
            note: action.note,
            price: action.price,
            discount: action.discount,
            rate: action.rate,
            class: action.class,
          })
          :
          state.items

      };
    case DELETE_ITEM:
      return {
        ...state,
        items: state.items.filter(item => {
          return item.id !== action.id;
        }),
        selectedItem: null
      };
    case SELECT_ITEM:
      return {
        ...state,
        selectedItem: state.items.find(item => {
          return item.id === action.id;
        })
      };
    case DESELECT_ITEM:
      return {
        ...state,
        selectedItem: null
      };
    case SET_CART_ITEMS:
      return {
        ...state,
        items: action.items
      };



    /// Wishlist /// 

    case ADD_WISH:
      if (state.wishItems.find((value, index) => { return value.id === action.id }) != null) {
        state.wishItems.filter((value, index) => { if (value.id === action.id) { return value.qty = action.qty + value.qty } else { return value.qty = value.qty } })
      }
      return {
        ...state,
        wishItems : state.wishItems.find((value, index) => { return value.id === action.id }) == null ?
          state.wishItems.concat({
            id: action.id,
            description: action.description,
            qty: action.qty,
            image: action.image,
            name: action.name,
            note: action.note,
            price: action.price,
            discount: action.discount,
            rate: action.rate,
            class: action.class,
          })
          :
          state.wishItems
          
      };
     case DELETE_WISH:
      return {
        ...state,
        wishItems: state.wishItems.filter(item => {
          return item.id !== action.id;
        }),
        selectedWish: null
      };

    case SELECT_WISH:
      return {
        ...state,
        wishItems: state.wishItems.find(item => {
          return item.id === action.id;
        })
      };

    case DESELECT_ITEM:
      return {
        ...state,
        selectedWish: null
      };

    case SET_CART_WISH:
      return {
        ...state,
        wishItems: action.items
      };



    default:
      return state;
  }
}
export default cart;
