import {ADD_ITEM, DELETE_ITEM, SELECT_ITEM, DESELECT_ITEM, SET_CART_ITEMS,ADD_WISH,DELETE_WISH,SELECT_WISH,DESELECT_WISH,SET_CART_WISH } from "./actionTypes";

export const addItem = (item) => {
    return {
        type: ADD_ITEM,
        //set item data
        id:item.id,
        qty:item.qty,
        note:item.note,
        description:item.description,
        image:item.image,
        name:item.name,
        price:item.price,
        discount:item.discount,
        rate:item.rate,
        class:item.class,

}
};
export const deleteItem = (item) => {
    return {
        type: DELETE_ITEM,
        // id to select item , qty to delete 
        id:item.id
    }
};
export const selectItem = (id) => {
    return {
        type: SELECT_ITEM,
        id: id
    }
};

export const deselectItem = () => {
    return {
        type: DESELECT_ITEM,


    }
};
export const setItems = (items) => {
    return {
        type: SET_CART_ITEMS,
        items: items,
    };

}

                                                                        // wishList // 




export const addWish = (item) => {
    return {
        type: ADD_WISH,
        //set item data
        id:item.id,
        qty:item.qty,
        note:item.note,
        description:item.description,
        image:item.image,
        name:item.name,
        price:item.price,
        discount:item.discount,
        rate:item.rate,
        class:item.class,

}
};
export const deleteWish = (item) => {
    return {
        type: DESELECT_WISH,
        // id to select item , qty to delete 
        id:item.id
    }
};
export const selectWish = (id) => {
    return {
        type: SELECT_WISH,
        id: id
    }
};

export const deselectWish = () => {
    return {
        type: DESELECT_WISH,


    }
};
export const setWishes = (items) => {
    return {
        type: SET_CART_WISH,
        items: items,
    };

}