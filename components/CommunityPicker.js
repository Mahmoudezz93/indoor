import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Picker,
  Text,
  View,
  Icon
} from "native-base";
import {Dimensions} from "react-native";
import {API,APIV,Governs,City1,City2} from "../functions/config";
import styles from "../screens/style";
import axios from "axios";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth =Dimensions.get("window").width;


export default class community extends Component {
  state = {
    governrates: [],
    cities: [],
    data: [],
    selected2: '',
    selected3: '',
    selectedGov:"",
    selectedCity:"",
    enabled:false, 
  };
  componentDidMount = async () => {
    this.get_govrns();
    const gov = this.props.gov 
    const city = this.props.city 
     
      this.setState({
        selectedGov:gov,
        selectedCity:city,
      })
    console.log("hahaha ", this.state.selectedGov)
    console.log("asasasasa ", this.state.selectedCity)

    //console.log("gov is ", this.props.gov)
   // console.log("city is ",this.props.city)
  }

  
   
  //requests
  get_govrns = async () => {
    await axios
      .get(API + APIV + "groups")
      .then(res => {
        console.log("success->getting govrns");
        this.setState({
          governrates: res.data.data.groups
        });
      }
      )
      .catch((error)=>{
        console.log(error);
      });
      console.log(this.state.governrates)
  };
  get_cities = async (id) => {
    await axios
      .get(API + APIV + City1 + id + City2)
      .then(res => {
        console.log("success->getting cities of " + id);
        this.setState({
          cities: res.data.data.cities
        });
      })
      .catch((error)=>{
        console.log(error);
      });
      ;
  };

  onValueChange2(value: string) {
    if (value == "0") {
      this.setState({
        selected3: "0",
        cities: []
      });
    }

    //console.log(value);
    this.setState({
      selected2: value
    });
    this.props.governChange(value.toString());

    if (value && value != "0") {
      this.setState({selectedGov:""})
      this.setState({selectedCity: ""})

      this.get_cities(value);
   
      this.setState({enabled:true})
    }
  }
  onValueChange3(value: string) {
    this.setState({
      selected3: value
    });
    this.props.cityChange(value.toString());
}

  render() {
     
    return (

<View>
<View style={[styles.profileInfoTab,{flexDirection:'row',
        alignItems:'center'  ,alignSelf:'center',justifyContent:'space-between',paddingHorizontal:0
        }]}>
        <Text style={{fontSize:15,color:'#fff',}} >    Choose Community : </Text>
        <Item  picker
        style={{minWidth:120,justifyContent:'flex-end', alignSelf:'flex-end',alignContent:'center',borderBottomWidth:0}}
        error={this.props.VGovern}>
          <Picker
            mode='dialog'
            placeholder= {""}
            placeholderStyle={{
              color: "#fff",
              fontSize:16
            }}

 
            placeholderIconColor="#007aff"
            selectedValue={ this.state.selected2   }
            onValueChange={this.onValueChange2.bind(this)}
            iosHeader={"  "}
            
            headerStyle={{alignSelf:'center' }}
            headerBackButtonText={'Back'}
            headerBackButtonTextStyle={{minWidth:50,fontWeight:'bold',color:'#fff'}}
          >
            <Picker.Item    label={this.state.selectedGov !== "" ? (this.state.selectedGov) :"  Community"} value="0" />
            {this.state.governrates.map(governrate => (
              <Picker.Item
                label={governrate.title}
                key={governrate.id}
                value={governrate.id}
                
              />
            ))}
          </Picker>
        </Item>
     </View>


  



</View>
       
       
     );
  }
}