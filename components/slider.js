import React, { PureComponent } from 'react';
import { Text, Dimensions, Image, StyleSheet, View } from 'react-native';

import SwiperFlatList from 'react-native-swiper-flatlist';
const titlelogo=require("../assets/icon.png");
 import stylessheet from"../screens/styles";

export default class App extends PureComponent {
  render() {
    return (
        <SwiperFlatList
          autoplay
          autoplayDelay={6}
          autoplayLoop
          index={0}
        >
          <View style={[styles.child, { backgroundColor: 'transparent' }]}>
          <Image source={titlelogo} style={stylessheet.titlelogo}/>
          </View>
          {/* <View style={[styles.child, { backgroundColor: 'transparent' }]}>
          <Image source={titlelogo} style={stylessheet.titlelogo}/>
          </View>
          <View style={[styles.child, { backgroundColor: 'transparent' }]}>
          <Image source={titlelogo} style={stylessheet.titlelogo}/>
          </View>
          <View style={[styles.child, { backgroundColor: 'transparent' }]}>
          <Image source={titlelogo} style={stylessheet.titlelogo}/>
          </View> */}
        </SwiperFlatList>
    );
  }
}

export const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  child: {
    height: height * 0.5,
    width,
    justifyContent: 'center'
  },
  text: {
    fontSize: width * 0.5,
    textAlign: 'center'
  }
});
