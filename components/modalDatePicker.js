import React, { useState } from "react";
import { Button, View, Dimensions ,Platform} from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import {Text } from 'native-base';


const Example = (props) => {

  const deviceHeight = Dimensions.get("window").height;
  const deviceWidth = Dimensions.get("window").width; 
  
  const [date, setDate] = useState(new Date());
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  
  if(props.date!=""||props.date!=null){
    console.log("date from parent : "+props.date);
    //this.props.dateChange(this.props.date);
  }  


  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (selectedDate) => {
    console.warn("A date has been picked: ", selectedDate);
    hideDatePicker();
    if (selectedDate!==""&&selectedDate!==null && selectedDate !== undefined)
    {
         const currentDate = selectedDate || date;
        
        setDate(currentDate);
        let date = currentDate;
            console.log("iam here "+date);
    
        let year=    currentDate.getFullYear();
        let month=    currentDate.getMonth() + 1;
        let day=    currentDate.getDate();
        console.log(year+"-"+month+"-"+day)
        props.dateChange(year+"-"+month+"-"+day);
        
    }
    else {
       

    }
  };

  return (
    <View style={{flex:1,alignContent:'center'}}>
        <View style={{flex:1,flexDirection:'row',justifyContent:'space-between',alignItems:'center',alignContent:'center'}}>
      <Text style={{fontSize:14,color:'#707070'}} >BIRTH</Text>
         <Text style={{backgroundColor:'#fff',flex:0.5,fontSize:16,textAlign:'right',
         color:(props.date ? '#000' : '#707070')
         ,fontWeight:'bold',paddingRight:6}}
          onPress={showDatePicker}>
         {`${props.date ?   props.date     : "Birth Date" }`}  
         </Text> 
         </View>
    

    <View>
       <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="date" 
        minimumDate={new Date(1930, 1, 1)}
        maximumDate={new Date()}
        date={date} 
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </View>
    </View>
  );
};

export default Example;