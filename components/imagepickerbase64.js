import React, { Component } from 'react';
import {  StyleSheet  } from 'react-native';
import { View, Text, Button, Image, Thumbnail} from 'native-base';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';

const cover = require("../assets/avatar.jpg");


export default class App extends Component {
  state = {
    pickerResult: cover,
  };
componentWillMount=()=>{
if(this.props.avatar!=""&&this.props.avatar!=null){
  this.setState({pickerResult:this.props.avatar})
}
}
  _pickImg = async () => {
    let pickerResult = await ImagePicker.launchImageLibraryAsync({
      base64: true,
      allowsEditing: false,
      mediaTypes : 'Images',
      aspect: [4, 3],
    });


    if (!pickerResult.cancelled) {
      this.setState({
        pickerResult,
      });
    let imageUri = pickerResult    
    if(imageUri!=cover && imageUri!=null && imageUri!="")
     {imageUri= `data:image/jpg;base64,${pickerResult.base64}`   
    }else{
      imageUri=""
    }
    console.log({ uri: imageUri.slice(0, 100) });
    this.props.imgChange(imageUri);
  }else{

  }
  };

  render() {
             let { pickerResult } = this.state;


             // <Button onPress={this._pickImg} title="Open Picker" />
             return (
               <Button
                 style={{
                   margin: 20
                 }}
                 transparent onPress={this._pickImg}>
                 <View style={{justifyContent:"center",alignItems:"center"}}>
                   <Thumbnail large source={!this.props.avatar? pickerResult: {uri:this.props.avatar}}
                  />
                 <Text>
                   Profile Picture 
                </Text>
                </View>
               </Button>
             );
           }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
});

/*
{
  pickerResult ? (
    <Text style={styles.paragraph}>
      Keys on pickerResult: {JSON.stringify(Object.keys(pickerResult))}
    </Text>
  ) : null;
} */
/* {
  pickerResult ? (
    <Image
      source={{ uri: imageUri }}
      style={{ width: 200, height: 200 }}
    />
  ) : null
} */