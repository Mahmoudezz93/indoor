import React, { Component } from 'react';
import { Container, Header, Content, DatePicker, Text } from 'native-base';
class DatePickerCustom extends Component {
    constructor(props) {
        super(props);
        this.state = { chosenDate: new Date() };
        this.setDate = this.setDate.bind(this);
    }
    componentWillMount=()=>{
        if(this.props.date!=""||this.props.date!=null){
            console.log("date from parent : "+this.props.date);
            this.props.dateChange(this.props.date);

        }
    }
    setDate(newDate) {
        this.setState({ chosenDate: newDate });
        console.log("iam here "+this.state.chosenDate);
        let date =this.state.chosenDate;

        let year=    date.getFullYear();
        let month=    date.getMonth() + 1;
        let day=    date.getDate();

        this.props.dateChange(year+"-"+month+"-"+day);
    }

    render() {
        return (
                    <DatePicker
                        defaultDate={new Date()}
                        minimumDate={new Date(1920, 1, 1)}
                         maximumDate={new Date()}
                        locale={"en"}
                        timeZoneOffsetInMinutes={undefined}
                        modalTransparent={false}
                        animationType={"fade"}
                        androidMode={"default"}
                        placeHolderText={this.props.date? this.props.date:" Birth Date*"}
                        textStyle={{ color: "#000" , fontSize:16,textAlign:'right',alignSelf:'flex-end' ,alignContent:'flex-end' }}
                        placeHolderTextStyle={{ alignSelf:'flex-end', color: "#000" ,fontSize : 17,textAlign:'right'}}
                        onDateChange={this.setDate}
                        
                        disabled={false}
                    />
        );
    }
}
export default DatePickerCustom;