import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { ParallaxImage } from 'react-native-snap-carousel';
import styles from '../styles/SliderEntry.style';
import { Rating, AirbnbRating } from 'react-native-ratings';
const Rating_img = require('../../../assets/ic_rating.png');

export default class SliderEntry extends Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
        even: PropTypes.bool,
        parallax: PropTypes.bool,
        parallaxProps: PropTypes.object
    };

    get image () {
        const { data: { illustration }, parallax, parallaxProps, even } = this.props;

        return parallax ? (
            <ParallaxImage
              source={{ uri: illustration }}
              containerStyle={[styles.imageContainer, even ? styles.imageContainerEven : {}]}
              style={styles.image}
              parallaxFactor={0.35}
              showSpinner={true}
              spinnerColor={even ? 'rgba(255, 255, 255, 0.4)' : 'rgba(0, 0, 0, 0.25)'}
              {...parallaxProps}
            />
        ) : (
            <Image
              source={{ uri: illustration }}
              style={styles.image}
            />
        );
    }

    render () {
        const { data: { title, subtitle,rate,price ,storeData}, even ,type } = this.props;
        const uppercaseTitle = title ? (
            <Text
              style={[styles.title, even ? styles.titleEven : {}]}
              numberOfLines={2}
            >
                { title.toUpperCase() }
            </Text>
        ) : false;

        return (
            <TouchableOpacity
              activeOpacity={1}
              style={ type=="store" ? [styles.storesSlideInnerContainer]:styles.slideInnerContainer}
              onPress={()=>
                type=="stores"?this.props.navigation.navigate("Store" ,{data:storeData})
                :this.props.navigation.navigate("ProductPage",{
                    data:storeData
                })
            }
              >
                <View style={type=="store"?styles.storeImageContainer:styles.imageContainer}>
                    { this.image }
                </View>

            {type!="store"? 
                <View style={[styles.textContainer, even ? styles.textContainerEven : {}]}>
                     

                        <Rating
                          
                          type='custom'
                          startingValue={rate}
                          readonly
                          fractions={2}
                          ratingImage={Rating_img}
                          ratingColor={rate < 3 ? '#FEC001' :rate <=6 ?'#FEC001':'#FEC001'}

                          ratingBackgroundColor='#c8c7c8'
                          ratingCount={5}
                          imageSize={28}
                          style={{ padding: 2, margin: 0 }}
                        />




                    { uppercaseTitle }
                    
                    <Text
                      style={[styles.subtitle, even ? styles.subtitleEven : {}]}
                      numberOfLines={2} 
                    >
                        { price  } EGP
                    </Text>
                   
                </View>
                :null }

            </TouchableOpacity>
        );
    }
}
