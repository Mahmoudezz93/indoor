import React, { Component } from 'react';
import { Platform, View, ScrollView, Text,  SafeAreaView ,Dimensions} from 'react-native';
import LinearGradient from 'expo-linear-gradient';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { sliderWidth, itemWidth ,storeItemWidth } from './styles/SliderEntry.style';
import SliderEntry from './components/SliderEntry';
import styles, { colors } from './styles/index.style';
import { ENTRIES1, ENTRIES2 } from './static/entries';
import { scrollInterpolators, animatedStyles } from './utils/animations';
const deviceHeight = Dimensions.get("window").height;
const deviceWidth =Dimensions.get("window").width;

const SLIDER_1_FIRST_ITEM = 1;

export default class Slider extends Component {

    constructor (props) {
        super(props);
        this.state = {
            slider1ActiveSlide: SLIDER_1_FIRST_ITEM,stocks:[]
        };
    }

    _renderItem ({item, index}) {
        return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
    }

    _renderItemWithParallax ({item, index}, parallaxProps) {
        return (
            <SliderEntry
              data={item}
              even={(index + 1) % 2 === 0}
              parallax={true}
              parallaxProps={parallaxProps}
            />
        );
    }

    _renderLightItem ({item, index}) {
        return <SliderEntry data={item} even={false} />;
    }

    _renderDarkItem ({item, index}) {
        return <SliderEntry data={item} even={true} />;
    }
    _renderStoreItem ({item, index}) {
        return <SliderEntry navigation={this.props.navigation}  data={item} type="store" even={false} />;
    }

    renderItems () {
        const { slider1ActiveSlide } = this.state;
        const {type} =this.props;
        let SlideData =this.props.data.map((value)=>{
            return (
              {
                storeData:value,
                title: value.name,
                rate:value.rate,
               // subtitle: value.description,
                price:value.price,
                illustration: this.props.imagelink+value.image
              }
            )
          })
        return (
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} 
            style={[styles.sliderContentContainer ,{paddingTop:20}]}>
            {SlideData.map((value,index)=>{
                return(
                <SliderEntry 
                key={index}
                navigation={this.props.navigation}  data={value} type={this.props.type} even={false} />
                )
            })}
 
            </ScrollView>
        );          

    }
//lifecycle
componentDidMount=()=>{

      }
  
    componentWillUnmount(){
        //cancel all sync func
        this._isMounted = false;
      
      }
    render () {
        const renderItems = this.renderItems();

        return (
            <SafeAreaView style={styles.safeArea}>
                <View style={{ justifyContent:"center"}}>
                    <View
                      style={styles.scrollview}
                      scrollEventThrottle={200}
                      directionalLockEnabled={true}
                    >
                        { renderItems }
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}
