import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Picker,
  Text,
  View
} from "native-base";
import {Dimensions} from "react-native";
import {API,APIV,Governs,City1,City2} from "../functions/config";
import styles from "../screens/style";
import axios from "axios";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth =Dimensions.get("window").width;


export default class PickerInputExample extends Component {
  state = {
    governrates: [],
    cities: [],
    data: [],
    selected2: '',
    selected3: ''
  };
  componentDidMount() {
    this.get_govrns();
  }
  //requests
  get_govrns = async () => {
    await axios
      .get(API + APIV + Governs)
      .then(res => {
        console.log("success->getting govrns");
        this.setState({
          governrates: res.data.data.governrates
        });
      }
      )
      .catch((error)=>{
        console.log(error);
      });
  };
  get_cities = async (id) => {
    await axios
      .get(API + APIV + City1 + id + City2)
      .then(res => {
        console.log("success->getting cities of " + id);
        this.setState({
          cities: res.data.data.cities
        });
      })
      .catch((error)=>{
        console.log(error);
      });
      ;
  };

  onValueChange2(value: string) {
    if (value == "0") {
      this.setState({
        selected3: "0",
        cities: []
      });
    }

    //console.log(value);
    this.setState({
      selected2: value
    });
    this.props.governChange(value.toString());

    if (value && value != "0") {
      this.get_cities(value);
    }
  }
  onValueChange3(value: string) {
    this.setState({
      selected3: value
    });
    this.props.cityChange(value.toString());
}

  render() {
    return (
      <View>
        <Item  picker
        style={{width:deviceWidth-20}}
         error={this.props.VGovern}>
          <Picker
            mode="dialog"
            placeholder="Select Govrernate"
            placeholderStyle={{
              color: "#555"
            }}
            iosHeader={"  "}
            
            headerStyle={{alignSelf:'center' }}
            headerBackButtonText={'Back'}
            headerBackButtonTextStyle={{minWidth:50,fontWeight:'bold'}}
            placeholderIconColor="#cdcdcd"
            selectedValue={this.state.selected2}
            onValueChange={this.onValueChange2.bind(this)}
          >
            <Picker.Item label="Select Governrate*"  value="0" />
            {this.state.governrates.map(governrate => (
              <Picker.Item
                label={governrate.en_name}
                key={governrate.id}
                value={governrate.id}
                
              />
            ))}
          </Picker>
        </Item>
        <Item  picker
        style={{width:deviceWidth-20 ,marginTop:10}}
         error={this.props.VCity}>
          <Picker
            mode="dialog"
            placeholder="Select City"
            placeholderStyle={{
              color: "#555"
            }}
            iosHeader={"  "}
            
            headerStyle={{alignSelf:'center' }}
            headerBackButtonText={'Back'}
            headerBackButtonTextStyle={{minWidth:50,fontWeight:'bold'}}
            placeholderIconColor="#007aff"
            selectedValue={this.state.selected3}
            onValueChange={this.onValueChange3.bind(this)}
          >
            <Picker.Item label="Select City*" value="0"  />
            {this.state.cities.map(city => (
              <Picker.Item label={city.en_name} key={city.id} value={city.id}  />
            ))}
          </Picker>
        </Item>
      </View>
    );
  }
}
