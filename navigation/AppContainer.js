import React, { Component } from "react";
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';


// Screens // 

import welcome from "../screens/welcomePage"; 
import Splash from "../screens/splash"

// Drawer // 
import SideBar from "../screens/Drawer/index";



// auth Stack / 
import Login from "../screens/auth/login";
import Signup from "../screens/auth/signup";
import CompeleteProfile from "../screens/auth/completeProfile";
import welcomePage from "../screens/welcomePage";

// Main Stack //
import HomeTabs from "../screens/Tabs/HomeTabs";
import Categories from "../screens/ecommerce/categories";
import SubCategories from "../screens/ecommerce/subCategories";
import ProductPage from "../screens/ecommerce/productPage";
import StorePage from "../screens/ecommerce/storePage";

import Profile from "../screens/Tabs/profile"
import Notifications from "../screens/profile/notifications"
import OrderHistory from "../screens/profile/orderHistory";
import TrackOrder from "../screens/profile/trackOrder";
import WishLists from "../screens/profile/wishLists";
import WishList from "../screens/profile/wishlist";


const MainStack = createStackNavigator({


  HomeTabs:{screen:HomeTabs},
  Categories:{screen:Categories},
  SubCategories:{screen:SubCategories},
  ProductPage:{screen:ProductPage},
  StorePage:{screen:StorePage},

  OrderHistory:{screen:OrderHistory},
  TrackOrder:{screen:TrackOrder},
  WishLists :{screen:WishLists},
  WishList:{screen:WishList},



}, { 
  initialRouteName: 'HomeTabs', 
  headerMode: "none"

});

 
const Drawer = createDrawerNavigator(
  {
    Home:{screen:MainStack},
    Profile:{screen:Profile},
    Notifications:{screen:Notifications},
  },
  {
    initialRouteName: "Home",
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    drawerPosition: "left",
    contentComponent: props => <SideBar {...props} />
  }
);


 


const LoginStack = createStackNavigator({
  Login:{screen:Login},
  SignUp:{screen:Signup},
  Drawer: { screen: Drawer },

}, {
  initialRouteName: "Login",
  headerMode: "none"

});

 
  export default createStackNavigator({
    
   Welcome:{screen:welcomePage},
   Splash:{screen:Splash},
   LoginStack:{screen:LoginStack},
   Drawer:{screen:Drawer},
  
  }, {
    initialRouteName: 'Splash',   // splash 
    headerMode: "none"
  
  })
   